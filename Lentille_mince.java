/*Cette classe s'occupe des lentilles minces : les lentilles convergentes et divergentes.
*/
public class LentilleMince extends ObjetOptique {

    public double distanceFocale ; // indique la distance focale de la lentille : peut être négative (lentille divergente) ou positive (lentille convergente).
    public final double TAILLE ; // indique la taille de la lentille, qui ne peut pas être modifiée.
    
}