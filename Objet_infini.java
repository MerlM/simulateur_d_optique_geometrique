/*Cette classe s'occupe des objets situés à l'infini sur l'axe optique.
*/
public class ObjetInfini extends Objet {

    /*Constructeur
    */
    public ObjetInfini (double p) {
        super(p) ;
    }

    /*Méthode toString
    */
    public String toString(){
        String s = ("...") ;
        return s ;
    }
        
    // méthode draw
    
}