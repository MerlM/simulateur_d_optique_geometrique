/*Cette classe est la classe mère pour les objet réels (objets et images) et les objets situés à l'infini.
*/
public abstract class Objet extends ObjetOptique {

    /*Constructeur
    */
    public Objet (double p) {
        super(p) ;
    }

    /*Méthode toString
    */
    public String toString(){
        String s = ("...") ;
        return s ;
    }
}