/*Cette classe est la classe mère pour tous les objets optiques à manipuler.
*/
public abstract class ObjetOptique {

    public double position ; // indique la position de l'objet optique sur l'axe optique

    /*Constructeur
    */
    public ObjetOptique (double p) {
        position = p ;
    }
    
}