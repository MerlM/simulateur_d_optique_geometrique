/*Cette classe s'occupe des objets réels : les objets et les images.
*/
public class ObjetReel extends Objet {

    public double taille ; // indique la taille de l'objet : elle peut être positive ou négative.

    /*Constructeur
    */
    public ObjetReel (double p, double t) {
        super(p) ;
        this.taille = t ;
    }

    /*Méthode toString
    */
    public String toString(){
        String s = ("...") ;
        return s ;
    }
    
    // méthode draw
    
}